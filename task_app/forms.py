from django import forms
from . import models

class FileUpload(forms.ModelForm):
    # pdf_file = forms.FileField(widget=forms.FileInput)
    class Meta:
        model = models.File
        fields = ('pdf_file',)