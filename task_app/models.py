from django.db import models

class File(models.Model):
    pdf_file = models.FileField(upload_to='pdf_files/')
    

    def __str__(self):
        return self.pdf_file.name

class FileMetadata(models.Model):
    file = models.ForeignKey('File',on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=255, null=True)
    path = models.CharField(max_length=255, null=True)
    size = models.FloatField(null=True)
    format = models.CharField(max_length=10,null=True)

    def __str__(self):
        return self.name