from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.Home.as_view(), name='home'),
    url(r'^upload/', views.upload_file, name='upload'),
    url(r'^preview/(?P<file_id>[0-9]+)$', views.pdf_view, name='preview'),
]