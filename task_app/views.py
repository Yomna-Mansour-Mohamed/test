from django.shortcuts import render, redirect
from django.forms.models import model_to_dict
from django.http import HttpResponse
from django.views.generic import TemplateView

from . import forms
from . import models

import base64

def get_files():
    files = models.FileMetadata.objects.all()
    # return model_to_dict(files.values()[0])
    return files

class Home(TemplateView):
    template_name = 'home.html'

def upload_file(request):
    if request.method == "POST":
        form = forms.FileUpload(request.POST, request.FILES)
        if form.is_valid():
            pdf_file = form.cleaned_data.get('pdf_file')
            pdf_file = base64.encodebytes(pdf_file.file.read())
            form.cleaned_data['pdf_file'] = pdf_file
            form.save()
            file = form.instance
            metadata = models.FileMetadata.objects.create(
                file=file,name=file.pdf_file.name.split('/')[1], path=file.pdf_file.path, 
                size=file.pdf_file.size,format=file.pdf_file.name.split('.')[1]
            )
            return render(request,'files_list.html',{'data':get_files()})

    return render(request,'file.html')


def pdf_view(request,file_id):
    file = models.File.objects.get(id=file_id)
    with open(file.pdf_file.path, 'rb') as pdf:
        response = HttpResponse(pdf.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'inline;filename=' + file.pdf_file.name
        return response
    pdf.closed